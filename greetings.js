const greetings = {
    hello: () => console.log('Hello'),
    helloName: (name) => console.log('Hello ' + name)
};

module.exports = greetings;